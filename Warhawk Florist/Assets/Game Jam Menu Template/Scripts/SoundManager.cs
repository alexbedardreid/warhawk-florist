﻿using UnityEngine;
using System.Collections;

namespace Sound
{
	public class SoundManager : MonoBehaviour
	{
		#region Script Instance
		private static SoundManager instance = null;

		private SoundManager() { }

		public static SoundManager Instance
		{
			get
			{
				return instance;
			}
		}

		void Awake()
		{
			if (SoundManager.Instance && !(SoundManager.Instance == this))
				gameObject.SetActive(false);
			else
				instance = this;

		}

		#endregion

		static AudioSource audioSource;

		public AudioClip HitSound;
		public AudioClip teleportSound;
		public AudioClip launchSound;
		public AudioClip poweringSound;
		public AudioClip clickSound;

		// Use this for initialization
		void Start()
		{
			audioSource = gameObject.GetComponent<AudioSource>();

			if (audioSource == null)
				audioSource = gameObject.AddComponent<AudioSource>();
		}

		public static void PlaySound(SOUNDS sound, float volume, bool withRand = false)
		{ instance.playSound(sound, volume, withRand); }

		public static void PlaySound(SOUNDS sound, bool withRand = false)
		{ instance.playSound(sound, 0.75f, withRand); }

		void playSound(SOUNDS sound, float volume, bool withRand)
		{
			if (!audioSource)
				return;

			AudioClip _temp = GetAudioClip(sound);

			if (!_temp)
				return;

			if(withRand)
			{
				audioSource.clip = _temp;
				audioSource.pitch = Random.Range(0.8f,1.2f);
				audioSource.Play();
			}
			else
				audioSource.PlayOneShot(_temp, volume);


		}

		AudioClip GetAudioClip(SOUNDS sound)
		{
			switch (sound)
			{
				case SOUNDS.hit:
					return HitSound;
				case SOUNDS.tele:
					return teleportSound;
				case SOUNDS.lnch:
					return launchSound;
				case SOUNDS.PWR:
					return poweringSound;
				case SOUNDS.CLK:
					return clickSound;
				default:
					throw new System.NotImplementedException("No sound setup for " + sound);
			}
		}

	}

	public enum SOUNDS
	{
		hit,
		tele,
		lnch,
		PWR,
		CLK
	}
}