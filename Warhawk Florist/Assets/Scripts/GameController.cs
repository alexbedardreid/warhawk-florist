﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Recycling;

public class GameController : MonoBehaviour
{

	#region Script Instance
	private static GameController instance = null;

	private GameController() { }

	public static GameController Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (GameController.Instance && !(GameController.Instance == this))
			gameObject.SetActive(false);
		else
			instance = this;

	}

	#endregion

	public BirdController controller;
	public Animating animator;

	[Space(10f)]
	public GameObject[] flyingObjects;

	[Space(10f)]
	public int currentMax = 5;
	public float spawnIncrement = 0.5f;
	public Vector3 flowerSpeed = Vector3.zero;
	public List<GameObject> ActiveFlowers;
	Dictionary<int, FLOWER> objTypes;

	public CameraShake camShake;

	[Space(10f), SerializeField]
	public float hitFlowerPauseTime = 0.06f;
	public bool hitFlower = false;
	public int hitFlowers = 0;
	public int streakHitCount = 0;

	public int _score = 0;

	int specialCount = 0;

	[Space(10f)]
	//Min 
	public Vector2 SpawnRange;

	float timePassed = 0f;

	List<GameObject> GarbagePile;

	// Use this for initialization
	void Start()
	{
		if (flyingObjects == null || flyingObjects.Length <= 0)
			throw new System.MissingFieldException("There are no flowers to spawn...");

		ActiveFlowers = new List<GameObject>();
		objTypes = new Dictionary<int, FLOWER>();

		CameraWorldBounds = Camera.main.GetWorldScreenCorners();

		if(!camShake)
			camShake = Camera.main.GetComponent<CameraShake>();

		UIManager.Instance.PauseEvent += Paused;
		UIManager.Instance.UnPauseEvent += UnPaused;

	}

	// Update is called once per frame
	void Update()
	{
		if (paused)
			return;

		if (timePassed >= spawnIncrement)
		{
			CheckSpawn();
			timePassed = 0f;
        }
		else
			timePassed += Time.deltaTime;

		if(!controller.PauseAttack && !controller.attackCountingDown)
		{
			for (int i = 0; i < ActiveFlowers.Count; i++)
			{
				if (!ActiveFlowers[i].activeInHierarchy)
					continue;

				if (ActiveFlowers[i].transform.position.x < -8f)
				{
					KillFlower(ActiveFlowers[i]);
					continue;
				}

				ActiveFlowers[i].transform.position += flowerSpeed;
			}
		}

		//if (ActiveFlowers.Count < )
	}

	public void ShakeCamera(float length, float amount = 0.7f)
	{
		camShake.shakeAmount = amount;
		camShake.shake = length;
	}
	public void GlitchCamera(float length)
	{
		camShake.GlitchScreen(length);
	}

	GameObject _goTemp;
	void CheckSpawn()
	{
		if (ActiveFlowers.Count < currentMax)
		{
			if((Random.value >= 0.95f) && !controller.travelingLines && specialCount < 1)
			{
				if(!Recycler.TryGrab(FLOWER.PICUP, out _goTemp))
				{
					_goTemp = Instantiate(flyingObjects[2]);
				}

				_goTemp.transform.position = new Vector3(8f, Random.Range(SpawnRange.x, SpawnRange.y));
				objTypes.Add(_goTemp.GetInstanceID(), FLOWER.PICUP);
				specialCount++;
            }
			else if(Random.value < 0.7f)
			{
				if(!Recycler.TryGrab(FLOWER.DEFAULT, out _goTemp))
				{
					_goTemp = Instantiate(flyingObjects[0]);
				}

				_goTemp.transform.position = new Vector3(8f, Random.Range(SpawnRange.x, SpawnRange.y));
				objTypes.Add(_goTemp.GetInstanceID(), FLOWER.DEFAULT);
			}
			else 
			{
				float _randVal = Random.value;
				if(!Recycler.TryGrab(FLOWER.THORN, out _goTemp))
				{
					_goTemp = Instantiate(flyingObjects[1]);
				}

				_goTemp.GetComponent<SpriteRenderer>().flipY = !(_randVal < 0.5f);
				_goTemp.transform.position = new Vector3(8f, (_randVal < 0.5f ? CameraWorldBounds[0].y + 0.75f : CameraWorldBounds[1].y - 0.75f));
				objTypes.Add(_goTemp.GetInstanceID(), FLOWER.THORN);
			}


			ActiveFlowers.Add(_goTemp);

		}
    }

	public void KillFlower(GameObject _obj)
	{
		if (!ActiveFlowers.Contains(_obj))
			return;

		FLOWER type = objTypes[_obj.GetInstanceID()];
		objTypes.Remove(_obj.GetInstanceID());

		if(type == FLOWER.PICUP)
			specialCount--;

		ActiveFlowers.Remove(_obj);
		ActiveFlowers.TrimExcess();
		Recycler.Recycle(type, _obj);
	}

	Vector2[] CameraWorldBounds;

	public bool InCameraBounds(Vector3 postion)
	{
		if(postion.x >= CameraWorldBounds[1].x ||
			postion.x < CameraWorldBounds[0].x ||
			postion.y >= CameraWorldBounds[1].y ||
			postion.y < CameraWorldBounds[0].y)
			return false;

		return true;
	}

	public void ClampPositionBounds(ref Vector3 _pos)
	{
		_pos.y = Mathf.Clamp(_pos.y, CameraWorldBounds[0].y, CameraWorldBounds[1].y);
		_pos.x = Mathf.Clamp(_pos.x, CameraWorldBounds[0].x, CameraWorldBounds[1].x);
	}

	public void AddToGarbagePile(GameObject garbage)
	{
		if (GarbagePile == null || GarbagePile.Count <= 0)
			GarbagePile = new List<GameObject>();

		GarbagePile.Add(garbage);
	}

	public void TossGarbagePile()
	{
		if (GarbagePile == null || GarbagePile.Count <= 0)
			return;

		for (int i = 0; i < GarbagePile.Count; i++)
			Destroy(GarbagePile[i]);

		GarbagePile.Clear();
	}


	public bool paused = false;
	public void Paused()
	{
		paused = true;
    }

	public void UnPaused()
	{
		paused = false;
	}

}
