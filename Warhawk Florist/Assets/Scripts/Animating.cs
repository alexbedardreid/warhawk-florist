﻿using UnityEngine;
using System.Collections;

public class Animating : MonoBehaviour
{
	[SerializeField]
	Animator animator;

	int[] _hash;


	public bool attacking
	{
		get { return _attacking; }
		set
		{
			_attacking = value;
			animator.SetBool(_hash[1], value);
		}
	}
	bool _attacking = false;
	//float _Acount = 0f;

	// Use this for initialization
	void Start()
	{
		if (animator == null)
			throw new System.NullReferenceException("No animator Connected...");

		_hash = new int[3]
		{
			Animator.StringToHash("Flap"),
			Animator.StringToHash("Attack"),
			Animator.StringToHash("AttackType")
		};
    }

	public void SetAttackType(int value)
	{
		if (value < 0)
			return;

		animator.SetInteger(_hash[2], value);
	}

	

	// Update is called once per frame
	void Update()
	{
		/*if(Input.GetKeyDown(KeyCode.D))
		{
			attacking = true;
			_Acount = 0f;
        }
		else if (Input.GetKeyUp(KeyCode.D))
		{
			attacking = false;
		}
		else */if (Input.GetKeyDown(KeyCode.Space))
		{
			animator.SetTrigger(_hash[0]);
		}

		//if (attacking && _Acount >= 1.5f)
		//{
		//	_Acount = 0f;
		//	attacking = false;
		//}
		//else
		//	_Acount += Time.deltaTime;


	}
}
