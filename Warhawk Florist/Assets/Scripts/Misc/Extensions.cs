﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

using Random = System.Random;

public static class TransformExtensions
{
	public static bool TryGetComponent(this Transform t, Type _typ, out Component comp)
	{
		comp = null;
		if (!t) return false;

		if ((t.GetComponent(_typ)))
		{
			comp = t.GetComponent(_typ);
			return true;
		}

		return false;
	}

	public static GameObject[] ChildrenGameObjects(this Transform t)
	{
		int _count = t.childCount;

		GameObject[] _children = new GameObject[_count];

		for (int i = 0; i < _count; i++)
		{
			_children[i] = t.GetChild(i).gameObject;
		}

		return _children;
	}

	public static Transform[] GetChildren(this Transform t)
	{
		int _count = t.childCount;

		Transform[] _children = new Transform[_count];

		for (int i = 0; i < _count; i++)
		{
			_children[i] = t.GetChild(i);
		}

		return _children;
	}

	public static Transform ChildrenGameObjects(this Transform t, string contains)
	{
		int _count = t.childCount;
		Transform _t;

		for (int i = 0; i < _count; i++)
		{
			_t = t.GetChild(i);
			if (_t.name.Contains(contains))
				return _t;
		}

		return null;
	}

	public static int GetActiveChildren(this Transform trans)
	{
		int _chldrn = trans.childCount;
		int count = 0;

		if (_chldrn <= 0) return 0;

		for (int i = 0; i < _chldrn; i++)
		{
			if (trans.GetChild(i).gameObject.activeSelf) count++;
		}

		return count;
	}
}

public static class ListExtensions
{
	public static void Shuffle<T>(this IList<T> list)
	{
		if (list == null)
		{
			Debug.LogError("Attempting to Shuffle empty list...");
			return;
		}

		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	public static class ThreadSafeRandom
	{
		[ThreadStatic]
		private static Random Local;

		public static Random ThisThreadsRandom
		{
			get { return Local ?? (Local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
		}
	}


}

public static class CameraExtensions
{
	//******Orthographic Camera Only******//

	public static Vector2 BoundsMin(this Camera cam)
	{
		return cam.transform.position - Extents(cam);
	}

	public static Vector2 BoundsMax(this Camera camera)
	{
		return camera.transform.position + Extents(camera);
	}

	public static Vector2[] GetWorldScreenCorners(this Camera cam)
	{
		Vector2[] _corners = new Vector2[2]
		{
			cam.BoundsMin(),
			cam.BoundsMax()
		};
			
		return _corners;

	}

	static	Vector3 Extents(Camera camera)
	{
		if (camera.orthographic)
			return new Vector3(camera.orthographicSize * Screen.width/Screen.height, camera.orthographicSize);
		else
		{
			Debug.LogError("Camera is not orthographic!", camera);
			throw new System.FormatException("Camera is not orthographic!");
		}
	}
	//*****End of Orthographic Only*****//
}