﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillParticles : MonoBehaviour
{

	List<ParticleSystem> _parts;

	// Use this for initialization
	void Start()
	{
		_parts = new List<ParticleSystem>();
		_parts.Add(gameObject.GetComponent<ParticleSystem>());
		_parts.AddRange(gameObject.GetComponentsInChildren<ParticleSystem>());

    }

	// Update is called once per frame
	void LateUpdate()
	{

		for(int i = 0; i < _parts.Count; i++)
		{
			if (_parts[i].particleCount > 0)
				return;
		}

		Destroy(this.gameObject);

	}
}
