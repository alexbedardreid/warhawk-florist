﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraShake : MonoBehaviour
{
	new public Camera camera;
	public float shake = 0f;
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1f;

	public GlitchEffect _glitch;

	Transform camTrans;
	Vector3 startPos;
	// Use this for initialization
	void Start()
	{
		if (!camera)
		{
			camera = this.GetComponent<Camera>();
			camTrans = camera.transform;
		}
		else
			camTrans = camera.transform;

		startPos = camTrans.position;

		if (_glitch == null)
			throw new System.NullReferenceException("No Glitch Effect...");
	}

	Vector3 _temp;
	// Update is called once per frame
	void Update()
	{
		if(shake > 0)
		{
			_temp = Vector3.Lerp(startPos, startPos + (Random.insideUnitSphere * shakeAmount), shake);
			_temp.z = startPos.z;

			camTrans.position = _temp;
			//camTrans.localPosition = Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * decreaseFactor * decreaseFactor;
		}
		else
		{
			shake = 0;
		}
	}

	public void GlitchScreen(float time)
	{
		if (glitching)
			return;

		StartCoroutine(GlitchTime(time));
	}

	bool glitching = false;
	IEnumerator GlitchTime(float time)
	{
		if (glitching)
			yield break;
		else
			glitching = true;

        float _inc = 0f;

		_glitch.enabled = true;

		while (_inc < time)
		{
			_inc += Time.deltaTime;

			yield return null;
		}

		_glitch.enabled = false;
		glitching = false;
    }

}