﻿using UnityEngine;
using System.Collections;

public enum FLOWER: int
{
	DEFAULT = 0,
	THORN = 1,
	PICUP = 2
}
