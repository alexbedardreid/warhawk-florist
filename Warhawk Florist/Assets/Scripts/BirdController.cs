﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sound;
using Recycling;

public class BirdController : MonoBehaviour
{
	public GameObject MultText;
	public GameObject ParticleHitEffect;
	public GameObject ParticleSmokeEffect;
	public GameObject ParticleBloodEffect;
	public GameObject ParticleSpecialEffect;
	List<GameObject> GarbagePile;

	[Space(10f)]
	public Vector2 yBounds = Vector2.zero;
	[SerializeField]
	float FlapClimb = 3f;
	[SerializeField]
	float FallRate = -1f;

	[Space(10f), SerializeField]
	Animating animator;

	[Space(10f), SerializeField]
	new Camera camera;

	[Space(10f), SerializeField]
	new SpriteRenderer renderer;

	[Space(10f), SerializeField]
	ParticleSystem partsSystem;

	new Collider2D collider;

	new Transform transform;
	Vector3 _tempPos = Vector3.zero;

	float _yDelta = 0f;

	#region Velocity 

	public Vector3 Velocity
	{
		get { return (currentPosition - lastPosition) / Time.deltaTime; }
	}
	Vector3 lastPosition = Vector3.zero;
	Vector3 currentPosition = Vector3.zero;
	#endregion

	#region Attacking Properties
	public bool PauseAttack 
	{
		get;
		private set;
	}

	Vector3 mousePos = Vector3.zero;
	List<Vector3> _positions;
	Vector3 nextPoint;
	public bool travelingLines
	{
		get;
		private set;
	}

	GameController _GameController
	{
		get
		{
			if(!_gameController)
				_gameController = GameController.Instance;

			return _gameController;
		}
	}
	GameController _gameController;


	int _GChitFlowers
	{
		get { return _gameController.hitFlowers; }
		set { _gameController.hitFlowers = value; }
	}

	public bool attackCountingDown { get; private set; }
	public float attackTime = 5f;
	float dashTimeCheck = 0f;
	bool dashing = false;
	bool needsCleaning = false;
	bool isCleaning = false;
	Queue<Vector3> dashDirections;

	#endregion

	// Use this for initialization
	void Start()
	{
		UIManager.Instance.ShowTutorial(UIManager.TUT.WASD);
		PauseAttack = false;

		dashDirections = new Queue<Vector3>();
		_positions = new List<Vector3>();
		transform = gameObject.transform;

		collider = GetComponent<BoxCollider2D>();

		if (animator == null)
			throw new System.NullReferenceException("No Animating Script...");

		if (camera == null)
			throw new System.NullReferenceException("No Camera...");

		attackCountingDown = false;
		travelingLines = false;
        UIManager.Instance.SetTime(0f);
	}

	void DashCheck(KeyCode key, Vector3 dir)
	{
		if (Input.GetKeyDown(key))
		{
			StartCoroutine(AttackDash(dir));
			//TODO Dont clear, but follow the path created!

			if(!needsCleaning)
				needsCleaning = true;

			//TODO Maybe some slowmo
			//attackCountingDown = false;
			//attackTime = 0f;
			dashTimeCheck = 0f;

			_yDelta = 0f;
        }
	}

	
	// Update is called once per frame
	void Update()
	{
		_tempPos = transform.position;
	
		#region Dash Moves

		DashCheck(KeyCode.D, Vector2.right);
		DashCheck(KeyCode.W, Vector2.up);
		DashCheck(KeyCode.S, -Vector2.up);
		DashCheck(KeyCode.A, -Vector2.right * 0.75f);

		if(!dashing && dashTimeCheck > 1f && needsCleaning && !attackCountingDown)
		{
			StartCoroutine(WaitToClean());
			//Debug.LogError("Time Clean Call");
		}

		if(dashing)
		{
			if(_GameController.hitFlower)
			{
				_GameController.streakHitCount++;
				StartCoroutine(HitFlowerEvent());
			}


		}

		#endregion

		//TODO Check for Powerup
		if (attackCountingDown && Input.GetMouseButtonDown(0))
		{

			if (_positions.Count <= 0)
				_positions.Add(transform.position);

			mousePos = Input.mousePosition;

			nextPoint = camera.ScreenToWorldPoint(mousePos);
			nextPoint.z = transform.position.z;
			_positions.Add(nextPoint);

			SoundManager.PlaySound(SOUNDS.CLK);

			OnRenderObject();
        }

		_yDelta += (FallRate * Time.deltaTime);

		if (Input.GetKeyDown(KeyCode.Space))
		{
			_yDelta += (FlapClimb * Time.deltaTime);
		}

		if (_yDelta > 0f && !_GameController.paused)
			_yDelta -= _yDelta / 5f;

		if (!_GameController.paused)
			_tempPos.y += _yDelta;
		else
			_yDelta = 0f;


		if (attackCountingDown && !_GameController.paused)
		{
			if (attackTime <= 0f)
			{
				attackCountingDown = false;
				attackTime = 2f;
				UIManager.Instance.SetTime(0f);
				TriggerLinesEvent();
            }
			else
			{
				UIManager.Instance.SetTime(attackTime -= Time.deltaTime);
				_tempPos.y = transform.position.y;
			}

		}
		else
		{
			_GameController.ClampPositionBounds(ref _tempPos);

		}

		dashTimeCheck += Time.deltaTime;
		transform.position = _tempPos;

    }

	#region Collision Detection

	void OnCollisionEnter2D(Collision2D collision)
	{

		CollisionCheck(collision.gameObject);

	}
	void OnCollisionExit2D(Collision2D collision)
	{

		CollisionCheck(collision.gameObject);

	}

	void CollisionCheck(GameObject _go)
	{
		if(_go.tag.Equals("Pickup"))
		{
			//Destroy(_go);

			_GameController.KillFlower(_go);

			SoundManager.PlaySound(SOUNDS.PWR);
			StartCoroutine(HitPickupEvent());
			//_GameController.ShakeCamera(0.7f, 1f);


		}
		else if(_go.tag.Equals("Thorn") && !travelingLines)
		{
			if(hittingThorn)
				return;
			
			StartCoroutine(HitThornEvent());
			_GameController.ShakeCamera(0.7f, 1f);
			_GameController.GlitchCamera(0.5f);


		}
		else if(_go.tag.Equals("Flower") && (dashing || travelingLines))
		{
			_GameController.hitFlower = true;


			if (_GChitFlowers == 0)
			{
				_GChitFlowers++;
				_GameController._score += 100;

				UIManager.Instance.Multiplierlabel.text = (_GameController._score).ToString();

			}
			else
			{
				_GChitFlowers++;
				_GameController._score += 100;

				UIManager.Instance.Multiplierlabel.text = (_GameController._score)+ " x" + (Mathf.RoundToInt(Mathf.Pow(_GChitFlowers, 1.5f)));

			}

			//TODO Pause on hit amougnst other things
			//Destroy(collision.gameObject);

			_GameController.ShakeCamera(0.5f);

			GameController.Instance.KillFlower(_go);

		}
	}

	#endregion

	#region Coroutines

	public float speed = 20f;
	float LineDistance;
	//Used for a quick forward attack
	IEnumerator AttackDash(Vector3 direction)
	{
		if (travelingLines)
			yield break;

		if(isCleaning)
			yield break;
		
		dashDirections.Enqueue(direction);

		if(dashing)
		{ 
			//TODO Add to a stack/queue
			yield break;
		}
		else 
			dashing = true;

		float lerp = 0f;
		float rate = 1f;

		Vector3 start;
		Vector3 end;
		Vector3 _tempDir;


		while(dashDirections.Count > 0)
		{
			_tempDir = dashDirections.Dequeue();
			lerp = 0f;
			rate = 1f;
			_GameController.streakHitCount = 0;

			start = transform.position;
			end = start + (_tempDir * 3f);

			LineDistance = Vector3.Distance(start, end);
			rate = (1f / LineDistance) * speed * 2f;

			//animator.attacking = true;
			PauseAttack = false;
			animator.attacking = true;

			SoundManager.PlaySound(SOUNDS.lnch, 0.1f);

			while (lerp < 1f)
			{

				if(!_GameController.InCameraBounds(transform.position = Vector3.Lerp(start,
					end, lerp += rate * Time.deltaTime)))
					break;

				if(_GameController.hitFlower)
				{
					while(_GameController.hitFlower)
						yield return null;
				}

				yield return null;
			}

			animator.attacking = false;

			if (_GameController.streakHitCount > 1)
				UIManager.Instance.score += (_GameController.streakHitCount * 500);
			
			_GameController.streakHitCount = 0;

			if(!_GameController.InCameraBounds(transform.position))
				break;
		}



		//animator.attacking = false;

		_positions.Clear();
		dashing = false;

		if(!_GameController.InCameraBounds(transform.position))
			StartCoroutine(WaitToClean());

    }

	//USed to travel along placed lines
	IEnumerator TravelLines()
	{
		if (travelingLines)
			yield break;
		else
			travelingLines = true;

        float lerp = 0f;
		float rate = 1f;
		int amountHit = 0;

		animator.attacking = true;
		PauseAttack = true;

		float accelSpeed = speed;

		for (int i = 0; i < _positions.Count; i++)
		{
			if (i + 1 >= _positions.Count)
				break;

			LineDistance = Vector3.Distance(_positions[i], _positions[i + 1]);

			rate = (1f / LineDistance) * (accelSpeed);
			accelSpeed += (accelSpeed / 4f);
			//Debug.LogError("Travel Rate: " + rate);

			lerp = 0f;

			ChangeSprite((_positions[i + 1].x < _positions[i].x),
				(_positions[i + 1].y < _positions[i].y));

			while (lerp < 1f)
			{
				transform.position = Vector3.Lerp(_positions[i],
					_positions[i+1], lerp += rate * Time.deltaTime);

				if (_GameController.hitFlower)
				{
					UIManager.Instance.score += ++amountHit * 10000;
					yield return StartCoroutine(HitFlowerEvent());
				}

				yield return null;
			}

			//yield return new WaitForSeconds(0.02f);

		}


		_GChitFlowers = 0;
		animator.attacking = false;
		PauseAttack = false;

		_positions.Clear();
		travelingLines = false;

		StartCoroutine(WaitToClean());

	}

	//In the event that a flower object was hit
	IEnumerator HitPickupEvent()
	{

		//TODO Add in some particle effects here for later.

		//partsSystem.Emit(10);

		_GameController.ShakeCamera(4f, 0.075f);
		attackTime = 2f;
		UIManager.Instance.SetTime(attackTime);


		if (!attackCountingDown)
		{
			attackCountingDown = true;
			PauseAttack = true;
        }

		GameObject _tt = Instantiate(ParticleSpecialEffect, transform.position, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		needsCleaning = false;

		//SoundManager.PlaySound(SOUNDS.hit, true);
		//SoundManager.PlaySound(SOUNDS.tele);

		UIManager.Instance.ShowTutorial(UIManager.TUT.CLCK);

		yield return  StartCoroutine(PauseNoFly(1f));
	}

	//In the event that a flower object was hit
	IEnumerator HitFlowerEvent()
	{
		//if (!_GameController.hitFlower)
		//	yield break;

		_GameController.hitFlower = false;

		//TODO Add in some particle effects here for later.

		//partsSystem.Emit(10);

		GameObject _tt = Instantiate(ParticleHitEffect, transform.position, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		SoundManager.PlaySound(SOUNDS.hit, true);
		SoundManager.PlaySound(SOUNDS.tele);


		yield return new WaitForSeconds(_GameController.hitFlowerPauseTime);
	}

	bool hittingThorn = false;
	//In the event that a flower object was hit
	IEnumerator HitThornEvent()
	{
		hittingThorn = true;
		_GameController.hitFlower = false;

		_GameController.TossGarbagePile();

		GameObject _tt = Instantiate(ParticleBloodEffect, transform.position, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		//TODO Need to add a sound effect here

		isCleaning = true;
		needsCleaning = false;

		Vector3 _tempPos;
		_tempPos = new Vector3(-5f, 0.75f, transform.position.z);


		_yDelta = 0f;
		transform.position = _tempPos;
		renderer.flipX = false;
		renderer.enabled = false;

		yield return new WaitForSeconds(0.5f);



		_tt = Instantiate(ParticleSmokeEffect, _tempPos, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		if(_GameController._score > 0)
		{
			UIManager.Instance.score += _GameController._score * (Mathf.RoundToInt(Mathf.Pow(_GChitFlowers, 1.5f)));
			//Debug.LogError("Clean Label...", UIManager.Instance.Multiplierlabel);

			_GameController._score = 0;
			_GChitFlowers = 0;
			UIManager.Instance.Multiplierlabel.text = "";


		}


		_yDelta = 0f;
		transform.position = _tempPos;
		renderer.flipX = false;
		SoundManager.PlaySound(SOUNDS.tele);
		renderer.enabled = true;

		dashDirections.Clear();

		isCleaning = false;
		hittingThorn = false;
		//transform.localScale = Vector3.one;
	}



	IEnumerator WaitToClean()
	{
		isCleaning = true;
		needsCleaning = false;

		Vector3 _tempPos;

		yield return new WaitForSeconds(0.2f);
		_GameController.TossGarbagePile();

		//TODO Move to normal position
		GameObject _tt = Instantiate(ParticleSmokeEffect, transform.position, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		SoundManager.PlaySound(SOUNDS.tele);

		renderer.enabled = false;
		yield return new WaitForSeconds(0.2f);

		_tempPos = new Vector3(-5f, 0.75f, transform.position.z);

		_tt = Instantiate(ParticleSmokeEffect, _tempPos, Quaternion.identity) as GameObject;
		_GameController.AddToGarbagePile(_tt);

		if(_GameController._score > 0)
		{
			UIManager.Instance.score += _GameController._score * (Mathf.RoundToInt(Mathf.Pow(_GChitFlowers, 1.5f)));
			//Debug.LogError("Clean Label...", UIManager.Instance.Multiplierlabel);

			_GameController._score = 0;
			_GChitFlowers = 0;
			UIManager.Instance.Multiplierlabel.text = "";


		}


		_yDelta = 0f;
        transform.position = _tempPos;
		renderer.flipX = false;
		SoundManager.PlaySound(SOUNDS.tele);
		renderer.enabled = true;

		dashDirections.Clear();

		isCleaning = false;
		//transform.localScale = Vector3.one;

	}

	IEnumerator PauseNoFly(float time)
	{
		float _inc = 0f;

		Vector3 _pos = transform.position;

		while(_inc < time)
		{
			_inc += Time.deltaTime;

			transform.position = _pos;
			_yDelta = 0;

			yield return null;
		}
	}
	#endregion


	void ChangeSprite(bool flipped, bool movingDown)
	{
		renderer.flipX = flipped;

		animator.SetAttackType((movingDown) ? 1 : 0);

	}

	void TriggerLinesEvent()
	{
		if (travelingLines)
			return;
		
		StartCoroutine(TravelLines());
		//TODO Dont clear, but follow the path created!
		
		OnRenderObject();
		
		//TODO Maybe some slowmo
		attackCountingDown = false;
		attackTime = 0f;
		UIManager.Instance.SetTime(0f);
	}

	#region Create & Draw lines

	static Material lineMaterial;
	static void CreateLineMaterial()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			//var shader = Shader.Find("Hidden/Internal-Colored");
			Shader shader = Shader.Find("Mobile/Particles/Additive");
			lineMaterial = new Material(shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Turn on alpha blending
			lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
			lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
			// Turn backface culling off
			lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			// Turn off depth writes
			lineMaterial.SetInt("_ZWrite", 0);
		}
	}



	Vector3 hdg;
	float distance;
	Vector3 dir;
	Vector3 test;
	// Will be called after all regular rendering is done
	public void OnRenderObject()
	{

		CreateLineMaterial();
		// Apply the line material
		lineMaterial.SetPass(0);
		GL.PushMatrix();

		for (int i = 0; i < _positions.Count; i++)
		{
			if (i + 1 >= _positions.Count)
				break;

			hdg = (_positions[i] - _positions[i+1]);
			distance = hdg.magnitude;
			dir = hdg / distance; // This is now the normalized direction.
			test = Vector3.Cross(dir, Vector3.forward);
			test /= 15f;



			// Draw lines
			GL.Begin(GL.QUADS);

			GL.Color(Color.white);
			// One vertex at transform position
			GL.Vertex(_positions[i] + test);
			GL.Vertex(_positions[i + 1] + test);

			// Another vertex at edge of circle
			GL.Vertex(_positions[i + 1] - test);
			GL.Vertex(_positions[i] - test);

			GL.End();
		}

		
		GL.PopMatrix();
	}
	#endregion

}
