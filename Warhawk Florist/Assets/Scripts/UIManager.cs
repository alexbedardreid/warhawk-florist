﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
	#region Script Instance
	private static UIManager instance = null;

	private UIManager() { }

	public static UIManager Instance
	{
		get
		{
			return instance;
		}
	}

	void Awake()
	{
		if (UIManager.Instance && !(UIManager.Instance == this))
			gameObject.SetActive(false);
		else
			instance = this;

	}

	#endregion

	public UnityEngine.UI.Text TimeLabel;
	public float time
	{
		set
		{
			if (value <= 0f)
				TimeLabel.text = "";
			else
				TimeLabel.text = System.Math.Round(value, 2).ToString();
		}
	}

	public UnityEngine.UI.Text ScoreLabel;
	public UnityEngine.UI.Text ScoreAddLabel;

	public UnityEngine.UI.Text Multiplierlabel;

	#region Tutorial

	[Space(15f)]
	public GameObject TutorialBGObject;
	public GameObject TutorialWasd;
	public GameObject Tutorialclick;
	TUT waitType;
	public enum TUT
	{
		WASD,
		CLCK
	}
	bool waiting = false;
	bool[] shownTut = new bool[2]
	{
			false, false
	};

	public delegate void MyDelegate();
	public MyDelegate PauseEvent;
	public MyDelegate UnPauseEvent;
	#endregion

	public int score
	{
		get
		{
			return _score;
		}
		set
		{
			//ScoreLabel.text = "Score: " + value;
			_score = value;

			if(!countingUp)
				StartCoroutine(CountAddScore());

			if(addingPoints)
			{
				StopCoroutine(AddPoints(0));
			}

			StartCoroutine(AddPoints(value - dispScore));
		}
	}

	int _score = 0;
	int dispScore =0;

	// Use this for initialization
	void Start()
	{
		score = 0;

		TutorialBGObject.SetActive(false);
		TutorialWasd.SetActive(false);
		Tutorialclick.SetActive(false);
	}

	void Update()
	{
		if(waiting)
		{
			switch (waitType)
			{
				case TUT.WASD:
					if (Input.anyKey)
						CloseTutorial();
					break;
				case TUT.CLCK:
					if (Input.GetMouseButtonDown(0))
						CloseTutorial();
					break;
			}
		}
	}

	public void SetTime(float currenttime)
	{
		time = currenttime;
    }

	bool addingPoints = false;
	IEnumerator AddPoints(int amount)
	{
		if(amount <= 0)
			yield break;

		addingPoints = true;

		ScoreAddLabel.color = Color.white;
		ScoreAddLabel.text = "+" + amount;

		float lerp = 0f;

		while(lerp < 1f)
		{
			ScoreAddLabel.color = Color.Lerp(Color.white, Color.clear, lerp += Time.deltaTime);

			yield return null;
		}

		addingPoints = false;
	}


	bool countingUp = false;
	IEnumerator CountAddScore()
	{
		if(countingUp)
			yield break;
		else
			countingUp = true;

		while(dispScore < _score)
		{
			dispScore += (_score - dispScore)/5;
			ScoreLabel.text = "Score: " + dispScore;


			yield return null;
		}

		dispScore = _score;
		ScoreLabel.text = "Score: " + dispScore;

		countingUp = false;
	}

	#region Tutorial

	public void ShowTutorial(TUT _type)
	{
		if (shownTut[(int)_type])
			return;

		switch (_type)
		{
			case TUT.WASD:
				TutorialBGObject.SetActive(true);
				TutorialWasd.SetActive(true);
				Tutorialclick.SetActive(false);
				break;

			case TUT.CLCK:
				TutorialBGObject.SetActive(true);
				TutorialWasd.SetActive(false);
				Tutorialclick.SetActive(true);
				break;
		}


		if (PauseEvent != null)
			PauseEvent();

		waiting = true;
		waitType = _type;
	}

	public void CloseTutorial()
	{
		if (!TutorialBGObject.activeInHierarchy)
		{
			waiting = false;
			return;
		}

		shownTut[(int)waitType] = true;

		waiting = true;
		TutorialBGObject.SetActive(false);
		TutorialWasd.SetActive(false);
		Tutorialclick.SetActive(false);

		Time.timeScale = 1f;

		if (UnPauseEvent != null)
			UnPauseEvent();
	}

	#endregion

}
